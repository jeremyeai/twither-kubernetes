Kubernetes .yaml files to run on a Kubernetes cluster.

Automatically pulls the API Backend and Postgres images from Docker Hub.
There are 2 .yaml files, 'postgres.yaml' for running the database service,
and 'twither.yaml' for running the API Backend

To run:
	- Have Kubernetes and kubectl installed and set up
	- Navigate Terminal to the folder containing the files in this repository.
	- Enter the following command to prepare the ConfigMap to run the database initialization:
		kubectl create create configmap inititdb-config --from-file=001schema.sql [--from-file=002data.sql]
		
		with the second --from-file argument being optional
		If run with the second file argument, the database will populate the tables with sample data.
	
	- To run the services, enter in the following command:
	
		kubectl create -f postgres.yaml -f twither.yaml

	

	- Once finished, to delete the services run:
		
		kubectl delete -f postgres.yaml -f twither.yaml

	- And to delete the ConfigMap, run:

		kubectl delete configmap initdb-config