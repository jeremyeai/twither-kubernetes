CREATE TABLE twither_user (
    id          SERIAL          PRIMARY KEY,
    username    VARCHAR(20)     NOT NULL,
    password    VARCHAR(20)     NOT NULL,
    firstname   VARCHAR(20),
    lastname    VARCHAR(20),
    email       VARCHAR(255)    NOT NULL,
    bio         VARCHAR(1023),
    UNIQUE(username)
);

CREATE TABLE tweet (
    id          SERIAL          PRIMARY KEY,
    content     VARCHAR(280)    NOT NULL,
    created     DATE            NOT NULL        DEFAULT now(),
    userid      INT             NOT NULL,
    replyid     INT,
    FOREIGN KEY(userid) REFERENCES twither_user(id) ON DELETE CASCADE,
    FOREIGN KEY(replyid) REFERENCES tweet(id) ON DELETE CASCADE
);

CREATE TABLE topic (
    id          SERIAL          PRIMARY KEY,
    topic       VARCHAR(20)     NOT NULL,
    UNIQUE(topic)
);

CREATE TABLE topicfollow (
    id          SERIAL          PRIMARY KEY,
    userid      INT             NOT NULL,
    topicid     INT             NOT NULL,
    UNIQUE(userid, topicid),
    FOREIGN KEY(userid) REFERENCES twither_user(id) ON DELETE CASCADE,
    FOREIGN KEY(topicid) REFERENCES topic(id) ON DELETE CASCADE
);

CREATE TABLE topicmention (
    id          SERIAL          PRIMARY KEY,
    tweetid     INT             NOT NULL,
    topicid     INT             NOT NULL,
    UNIQUE(tweetid, topicid),
    FOREIGN KEY(topicid) REFERENCES topic(id) ON DELETE CASCADE,
    FOREIGN KEY(tweetid) REFERENCES tweet(id) ON DELETE CASCADE
);

CREATE TABLE tweetlike (
    id          SERIAL          PRIMARY KEY,
    userid      INT             NOT NULL,
    tweetid     INT             NOT NULL,
    created     DATE            NOT NULL        DEFAULT now(),
    UNIQUE(userid, tweetid),
    FOREIGN KEY(userid) REFERENCES twither_user(id) ON DELETE CASCADE,
    FOREIGN KEY(tweetid) REFERENCES tweet(id) ON DELETE CASCADE
);

CREATE TABLE tweetshare (
    id          SERIAL          PRIMARY KEY,
    userid      INT             NOT NULL,
    tweetid     INT             NOT NULL,
    created     DATE            NOT NULL        DEFAULT now(),
    UNIQUE(userid, tweetid),
    FOREIGN KEY(userid) REFERENCES twither_user(id) ON DELETE CASCADE,
    FOREIGN KEY(tweetid) REFERENCES tweet(id) ON DELETE CASCADE
);

CREATE TABLE userfollow (
    id          SERIAL          PRIMARY KEY,
    sourceid    INT             NOT NULL,
    targetid    INT             NOT NULL,
    UNIQUE(sourceid, targetid),
    FOREIGN KEY(sourceid) REFERENCES twither_user(id) ON DELETE CASCADE,
    FOREIGN KEY(targetid) REFERENCES twither_user(id) ON DELETE CASCADE
);

CREATE TABLE usermention(
    id          SERIAL          PRIMARY KEY,
    tweetid     INT             NOT NULL,
    userid      INT             NOT NULL,
    UNIQUE(tweetid, userid),
    FOREIGN KEY(tweetid) REFERENCES tweet(id) ON DELETE CASCADE,
    FOREIGN KEY(userid) REFERENCES twither_user(id) ON DELETE CASCADE
);


